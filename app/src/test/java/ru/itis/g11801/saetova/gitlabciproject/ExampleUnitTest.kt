package ru.itis.g11801.saetova.gitlabciproject

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun addition_isCorrect1() {
        assertEquals(1, 0 + 1)
    }

    @Test
    fun addition_isCorrect2() {
        assertEquals(2, 2 + 0)
    }

    @Test
    fun addition_isCorrect3() {
        assertEquals(3, 2 + 1)
    }

    @Test
    fun addition_isCorrectN() {
        assertEquals(2, 2 + 0)
    }

    @Test
    fun addition_isCorrectNQ() {
        assertEquals(1, 2 - 1)
    }
}
